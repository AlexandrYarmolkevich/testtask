window.onload = function () {
    let canvas = document.getElementById('canv') as HTMLCanvasElement;
    if (canvas && canvas.getContext) {
        let ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        if (ctx) {
            let width: number = canvas.width = window.innerWidth - 10;
            let height: number = canvas.height = window.innerHeight - 10;
            let isDragging = false;
            let delta: {
                x: number,
                y: number
            } = {
                x: 1,
                y: 2
            }
            class figures {
                x: number;
                y: number;
                color: string;
                points: any[];
                offset: any[];
                bool: boolean;
                constructor(x: number, y: number, color: string, points: any[], offset: any[], bool: boolean) {
                    this.x = x;
                    this.y = y;
                    this.color = color;
                    this.points = points;
                    this.offset = offset;
                    this.bool = false;
                }
                drawPoligon() {
                    ctx.beginPath();
                    ctx.fillStyle = this.color ? this.color : this.color = "white";
                    ctx.moveTo(this.points[0].x, this.points[0].y);
                    for (let i = 1; i < this.points.length; i++) {
                        ctx.lineTo(this.points[i].x, this.points[i].y);
                    }
                    ctx.closePath();
                    ctx.fill();
                }
                updatePoligon() {
                    for (let i = 0; i < this.points.length; i++) {
                        this.points[i].x = this.x + this.offset[i].x;
                        this.points[i].y = this.y + this.offset[i].y;
                    }
                }
            }

            let ArrayOfFigures = new Array < figures > ();
            ArrayOfFigures[0] = new figures(50, 100, "#FFFFFF", [{
                    'x': 0,
                    'y': 0
                },
                {
                    'x': 0,
                    'y': 0
                },
                {
                    'x': 0,
                    'y': 0
                },
                {
                    'x': 0,
                    'y': 0
                },
            ], [{
                    'x': -40,
                    'y': 40
                },
                {
                    'x': 40,
                    'y': 40
                },
                {
                    'x': 40,
                    'y': -40
                },
                {
                    'x': -40,
                    'y': -40
                },
            ], false);

            ArrayOfFigures[1] = new figures(50, 200, "#FFFFFF", [{
                    'x': 0,
                    'y': 0
                },
                {
                    'x': 0,
                    'y': 0
                },
                {
                    'x': 0,
                    'y': 0
                },
                {
                    'x': 0,
                    'y': 0
                },
            ], [{
                    'x': 60,
                    'y': -48
                },
                {
                    'x': 40,
                    'y': 48
                },
                {
                    'x': -40,
                    'y': 20
                },
                {
                    'x': -35,
                    'y': -28
                },
            ], false);

            ArrayOfFigures[2] = new figures(50, 300, "#FFFFFF", [{
                    'x': 0,
                    'y': 0
                },
                {
                    'x': 0,
                    'y': 0
                },
                {
                    'x': 0,
                    'y': 0
                },
            ], [{
                    'x': -40,
                    'y': 50
                },
                {
                    'x': 0,
                    'y': -40
                },
                {
                    'x': 55,
                    'y': 60
                },
            ], false);
            for (let i = 0; i < ArrayOfFigures.length; i++) {
                ArrayOfFigures[i].updatePoligon();
                ArrayOfFigures[i].drawPoligon();
            }

            let oMousePos = function (canvas: HTMLCanvasElement, evt: MouseEvent) {
                let rect = canvas.getBoundingClientRect();
                return {
                    x: evt.clientX - rect.left,
                    y: evt.clientY - rect.top
                };
            }

            let checkStarCollision = function (starA: figures, starB: figures) {
                for (let i = 0; i < starA.points.length; i++) {
                    let p0 = starA.points[i],
                        p1 = starA.points[(i + 1) % starA.points.length];
                    for (let j = 0; j < starB.points.length; j++) {
                        let p2 = starB.points[j],
                            p3 = starB.points[(j + 1) % starB.points.length];
                        if (segmentIntersect(p0, p1, p2, p3)) {
                            return true;
                        }
                    }
                }
                return false;
            }

            let segmentIntersect = function (p0: any, p1: any, p2: any, p3: any) {
                let A1 = p1.y - p0.y,
                    B1 = p0.x - p1.x,
                    C1 = A1 * p0.x + B1 * p0.y,
                    A2 = p3.y - p2.y,
                    B2 = p2.x - p3.x,
                    C2 = A2 * p2.x + B2 * p2.y,
                    denominator = A1 * B2 - A2 * B1;
                if (denominator == 0) {
                    return null;
                }
                let intersectX = (B2 * C1 - B1 * C2) / denominator,
                    intersectY = (A1 * C2 - A2 * C1) / denominator,
                    rx0 = (intersectX - p0.x) / (p1.x - p0.x),
                    ry0 = (intersectY - p0.y) / (p1.y - p0.y),
                    rx1 = (intersectX - p2.x) / (p3.x - p2.x),
                    ry1 = (intersectY - p2.y) / (p3.y - p2.y);
                if (((rx0 >= 0 && rx0 <= 1) || (ry0 >= 0 && ry0 <= 1)) &&
                    ((rx1 >= 0 && rx1 <= 1) || (ry1 >= 0 && ry1 <= 1))) {
                    return {
                        x: intersectX,
                        y: intersectY
                    };
                } else {
                    return null;
                }
            }

            canvas.addEventListener('mousedown', function (evt) {
                let mousePos = oMousePos(canvas, evt);
                for (let i = 0; i < ArrayOfFigures.length; i++) {
                    ArrayOfFigures[i].drawPoligon();
                    if (ctx.isPointInPath(mousePos.x, mousePos.y)) {
                        isDragging = true;
                        ArrayOfFigures[i].bool = true;
                        delta.x = ArrayOfFigures[i].x - mousePos.x;
                        delta.y = ArrayOfFigures[i].y - mousePos.y;
                        break;
                    } else {
                        ArrayOfFigures[i].bool = false;
                    }
                }
            }, false);

            canvas.addEventListener('mousemove', function (evt) {
                if (isDragging) {
                    let mousePos = oMousePos(canvas, evt);
                    for (let i = 0; i < ArrayOfFigures.length; i++) {
                        if (ArrayOfFigures[i].bool) {
                            ArrayOfFigures[i].updatePoligon();
                            ctx.clearRect(0, 0, width, height);
                            let X = mousePos.x + delta.x,
                                Y = mousePos.y + delta.y;
                            ArrayOfFigures[i].x = X;
                            ArrayOfFigures[i].y = Y;
                            break;
                        }
                    }

                    for (let i = 0; i < ArrayOfFigures.length; i++) {
                        ArrayOfFigures[i].drawPoligon();
                    }
                }
            }, false);

            canvas.addEventListener('mouseup', function (evt) {
                let figuresCount = [];
                for (let i = 0; i < ArrayOfFigures.length; i++) {
                    figuresCount[i] = 0;
                }
                isDragging = false;
                for (let i = 0; i < ArrayOfFigures.length; i++) {
                    ArrayOfFigures[i].bool = false
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                }
                for (let v = 0; v < ArrayOfFigures.length - 1; v++) {
                    for (let i = v + 1; i < ArrayOfFigures.length; i++) {
                        if (checkStarCollision(ArrayOfFigures[v], ArrayOfFigures[i])) {
                            figuresCount[v] += 1;
                            figuresCount[i] += 1;
                        }
                    }
                }
                for (let i = 0; i < figuresCount.length; i++) {
                    if (figuresCount[i] > 0) {
                        ArrayOfFigures[i].color = "red";
                        ArrayOfFigures[i].drawPoligon();
                    } else {
                        ArrayOfFigures[i].color = "white";
                        ArrayOfFigures[i].drawPoligon();

                    }
                }
            }, false);
        }
    }
}