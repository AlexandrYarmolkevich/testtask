'use strict';

window.onload = function () {
    var canvas = document.getElementById('canv');
    if (canvas && canvas.getContext) {
        var ctx_1 = canvas.getContext('2d');
        if (ctx_1) {
            var width_1 = canvas.width = window.innerWidth - 10;
            var height_1 = canvas.height = window.innerHeight - 10;
            var isDragging_1 = false;
            var delta_1 = {
                x: 1,
                y: 2
            };
            var figures = /** @class */function () {
                function figures(x, y, color, points, offset, bool) {
                    this.x = x;
                    this.y = y;
                    this.color = color;
                    this.points = points;
                    this.offset = offset;
                    this.bool = false;
                }
                figures.prototype.drawPoligon = function () {
                    ctx_1.beginPath();
                    ctx_1.fillStyle = this.color ? this.color : this.color = "white";
                    ctx_1.moveTo(this.points[0].x, this.points[0].y);
                    for (var i = 1; i < this.points.length; i++) {
                        ctx_1.lineTo(this.points[i].x, this.points[i].y);
                    }
                    ctx_1.closePath();
                    ctx_1.fill();
                };
                figures.prototype.updatePoligon = function () {
                    for (var i = 0; i < this.points.length; i++) {
                        this.points[i].x = this.x + this.offset[i].x;
                        this.points[i].y = this.y + this.offset[i].y;
                    }
                };
                return figures;
            }();
            var ArrayOfFigures_1 = new Array();
            ArrayOfFigures_1[0] = new figures(50, 100, "#FFFFFF", [{
                'x': 0,
                'y': 0
            }, {
                'x': 0,
                'y': 0
            }, {
                'x': 0,
                'y': 0
            }, {
                'x': 0,
                'y': 0
            }], [{
                'x': -40,
                'y': 40
            }, {
                'x': 40,
                'y': 40
            }, {
                'x': 40,
                'y': -40
            }, {
                'x': -40,
                'y': -40
            }], false);
            ArrayOfFigures_1[1] = new figures(50, 200, "#FFFFFF", [{
                'x': 0,
                'y': 0
            }, {
                'x': 0,
                'y': 0
            }, {
                'x': 0,
                'y': 0
            }, {
                'x': 0,
                'y': 0
            }], [{
                'x': 60,
                'y': -48
            }, {
                'x': 40,
                'y': 48
            }, {
                'x': -40,
                'y': 20
            }, {
                'x': -35,
                'y': -28
            }], false);
            ArrayOfFigures_1[2] = new figures(50, 300, "#FFFFFF", [{
                'x': 0,
                'y': 0
            }, {
                'x': 0,
                'y': 0
            }, {
                'x': 0,
                'y': 0
            }], [{
                'x': -40,
                'y': 50
            }, {
                'x': 0,
                'y': -40
            }, {
                'x': 55,
                'y': 60
            }], false);
            for (var i = 0; i < ArrayOfFigures_1.length; i++) {
                ArrayOfFigures_1[i].updatePoligon();
                ArrayOfFigures_1[i].drawPoligon();
            }
            var oMousePos_1 = function oMousePos_1(canvas, evt) {
                var rect = canvas.getBoundingClientRect();
                return {
                    x: evt.clientX - rect.left,
                    y: evt.clientY - rect.top
                };
            };
            var checkStarCollision_1 = function checkStarCollision_1(starA, starB) {
                for (var i = 0; i < starA.points.length; i++) {
                    var p0 = starA.points[i],
                        p1 = starA.points[(i + 1) % starA.points.length];
                    for (var j = 0; j < starB.points.length; j++) {
                        var p2 = starB.points[j],
                            p3 = starB.points[(j + 1) % starB.points.length];
                        if (segmentIntersect_1(p0, p1, p2, p3)) {
                            return true;
                        }
                    }
                }
                return false;
            };
            var segmentIntersect_1 = function segmentIntersect_1(p0, p1, p2, p3) {
                var A1 = p1.y - p0.y,
                    B1 = p0.x - p1.x,
                    C1 = A1 * p0.x + B1 * p0.y,
                    A2 = p3.y - p2.y,
                    B2 = p2.x - p3.x,
                    C2 = A2 * p2.x + B2 * p2.y,
                    denominator = A1 * B2 - A2 * B1;
                if (denominator == 0) {
                    return null;
                }
                var intersectX = (B2 * C1 - B1 * C2) / denominator,
                    intersectY = (A1 * C2 - A2 * C1) / denominator,
                    rx0 = (intersectX - p0.x) / (p1.x - p0.x),
                    ry0 = (intersectY - p0.y) / (p1.y - p0.y),
                    rx1 = (intersectX - p2.x) / (p3.x - p2.x),
                    ry1 = (intersectY - p2.y) / (p3.y - p2.y);
                if ((rx0 >= 0 && rx0 <= 1 || ry0 >= 0 && ry0 <= 1) && (rx1 >= 0 && rx1 <= 1 || ry1 >= 0 && ry1 <= 1)) {
                    return {
                        x: intersectX,
                        y: intersectY
                    };
                } else {
                    return null;
                }
            };
            canvas.addEventListener('mousedown', function (evt) {
                var mousePos = oMousePos_1(canvas, evt);
                for (var i = 0; i < ArrayOfFigures_1.length; i++) {
                    ArrayOfFigures_1[i].drawPoligon();
                    if (ctx_1.isPointInPath(mousePos.x, mousePos.y)) {
                        isDragging_1 = true;
                        ArrayOfFigures_1[i].bool = true;
                        delta_1.x = ArrayOfFigures_1[i].x - mousePos.x;
                        delta_1.y = ArrayOfFigures_1[i].y - mousePos.y;
                        break;
                    } else {
                        ArrayOfFigures_1[i].bool = false;
                    }
                }
            }, false);
            canvas.addEventListener('mousemove', function (evt) {
                if (isDragging_1) {
                    var mousePos = oMousePos_1(canvas, evt);
                    for (var i = 0; i < ArrayOfFigures_1.length; i++) {
                        if (ArrayOfFigures_1[i].bool) {
                            ArrayOfFigures_1[i].updatePoligon();
                            ctx_1.clearRect(0, 0, width_1, height_1);
                            var X = mousePos.x + delta_1.x,
                                Y = mousePos.y + delta_1.y;
                            ArrayOfFigures_1[i].x = X;
                            ArrayOfFigures_1[i].y = Y;
                            break;
                        }
                    }
                    for (var i = 0; i < ArrayOfFigures_1.length; i++) {
                        ArrayOfFigures_1[i].drawPoligon();
                    }
                }
            }, false);
            canvas.addEventListener('mouseup', function (evt) {
                var figuresCount = [];
                for (var i = 0; i < ArrayOfFigures_1.length; i++) {
                    figuresCount[i] = 0;
                }
                isDragging_1 = false;
                for (var i = 0; i < ArrayOfFigures_1.length; i++) {
                    ArrayOfFigures_1[i].bool = false;
                    ctx_1.clearRect(0, 0, canvas.width, canvas.height);
                }
                for (var v = 0; v < ArrayOfFigures_1.length - 1; v++) {
                    for (var i = v + 1; i < ArrayOfFigures_1.length; i++) {
                        if (checkStarCollision_1(ArrayOfFigures_1[v], ArrayOfFigures_1[i])) {
                            figuresCount[v] += 1;
                            figuresCount[i] += 1;
                        }
                    }
                }
                for (var i = 0; i < figuresCount.length; i++) {
                    if (figuresCount[i] > 0) {
                        ArrayOfFigures_1[i].color = "red";
                        ArrayOfFigures_1[i].drawPoligon();
                    } else {
                        ArrayOfFigures_1[i].color = "white";
                        ArrayOfFigures_1[i].drawPoligon();
                    }
                }
            }, false);
        }
    }
};